package com.yami.data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Student {
    @Id
    private int sid;
    private String name;

    public Student() {
        super();
        // TODO Auto-generated constructor stub
    }

    public Student(int id, String name) {
        this.sid = id;
        this.name = name;

    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
