package com.yami.data;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class TestClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Student s1 = new Student(); 
		s1.setSid(121);
		s1.setName("John");
		
		SessionFactory sessionFac = new Configuration().configure().buildSessionFactory();
		Session sess = sessionFac.openSession();
		sess.beginTransaction(); 
		sess.save(s1);
		sess.getTransaction().commit();
		sess.close();
		sessionFac.close();
		

	}

}
